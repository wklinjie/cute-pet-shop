package cn.tedu.cutepetboot.mapper;

import cn.tedu.cutepetboot.pojo.vo.EvaluationVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class MapperTest {
    @Autowired
    EvaluationMapper mapper;

    @Test
    void getByGoodsId(){
        Integer id =1;
        List<EvaluationVO> byGoodsId = mapper.getByGoodsId(id);
        for (EvaluationVO userEvaluationVOS : byGoodsId){
            System.out.println(userEvaluationVOS);
        }
    }
}

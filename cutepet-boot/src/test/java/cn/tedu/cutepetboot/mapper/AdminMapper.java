package cn.tedu.cutepetboot.mapper;

import cn.tedu.cutepetboot.pojo.entity.Admin;

public interface AdminMapper {
    int insert(Admin admin);
}

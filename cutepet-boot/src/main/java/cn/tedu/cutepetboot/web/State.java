package cn.tedu.cutepetboot.web;

public enum State {

    OK(200),
    ERR_USERNAME(201),
    ERR_PASSWORD(202);

    private Integer value;

    State(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

}

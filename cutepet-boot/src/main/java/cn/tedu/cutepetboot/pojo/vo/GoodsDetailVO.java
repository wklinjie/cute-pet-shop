package cn.tedu.cutepetboot.pojo.vo;

import lombok.Data;

@Data
public class GoodsDetailVO {
    private String attributeUrl;
    private String introduceUrl;
}

package cn.tedu.cutepetboot.pojo.vo;

import lombok.Data;

@Data
public class ProductListVO {
    private Integer id;
    private String name;
    private String imageUrl;
    private Double price;
    private Integer heat;
}

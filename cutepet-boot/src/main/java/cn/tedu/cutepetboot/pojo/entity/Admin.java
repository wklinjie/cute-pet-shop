package cn.tedu.cutepetboot.pojo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
public class Admin implements Serializable {
    private Integer id;
    private String username;
    private String password;
    private String name;
    private Date createTime;
    private Date updateTime;
}
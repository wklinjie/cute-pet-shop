package cn.tedu.cutepetboot.pojo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
public class GoodsReturned implements Serializable {
    private Integer id;
    private Integer orderId;
    private Integer userId;
    private Integer goodsId;
    private String goodsName;
    private Integer goodsNumber;
    private Double orderMoney;
    private Integer state;
    private Date createTime;
    private Date updateTime;


}
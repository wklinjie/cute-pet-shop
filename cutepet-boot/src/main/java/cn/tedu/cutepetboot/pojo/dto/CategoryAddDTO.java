package cn.tedu.cutepetboot.pojo.dto;

import lombok.Data;

import java.io.Serializable;
@Data
public class CategoryAddDTO implements Serializable {
    private String name;
    private Integer parentId;
}

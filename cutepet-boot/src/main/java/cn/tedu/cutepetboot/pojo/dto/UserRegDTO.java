package cn.tedu.cutepetboot.pojo.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserRegDTO implements Serializable {
    private String username;
    private String password;
    private String nickname;
    private String email;
}

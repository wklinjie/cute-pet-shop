package cn.tedu.cutepetboot.pojo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ShoppingCart implements Serializable {
    private Integer id;
    private Integer userId;
    private Integer goodsId;
    private String goodsName;
    private String goodsImg;
    private Integer count;
    private Integer status;
    private Date createTime;
    private Date updateTime;


}
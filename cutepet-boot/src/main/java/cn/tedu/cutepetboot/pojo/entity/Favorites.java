package cn.tedu.cutepetboot.pojo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Favorites implements Serializable {
    private Integer id;
    private Integer uid;
    private Integer goodsId;
    private Integer status;
    private Date createTime;
    private Date updateTime;


}
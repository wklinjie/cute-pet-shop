package cn.tedu.cutepetboot.pojo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class UserOrder implements Serializable {
    private Integer id;
    private Integer userId;
    private String payMoney;
    private String state;
    private String sendAdress;
    private Date sendTime;
    private Date createTime;
    private Date updateTime;


}
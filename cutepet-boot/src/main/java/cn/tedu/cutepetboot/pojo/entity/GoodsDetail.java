package cn.tedu.cutepetboot.pojo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
public class GoodsDetail implements Serializable {
    private Long id;
    private Long goodsId;
    private String attributeUrl;
    private String introduceUrl;
    private Boolean state;
    private Date createTime;
    private Date updateTime;


}
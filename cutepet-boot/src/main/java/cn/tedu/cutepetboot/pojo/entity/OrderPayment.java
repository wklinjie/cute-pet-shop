package cn.tedu.cutepetboot.pojo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class OrderPayment implements Serializable {
    private Integer id;
    private Integer orderId;
    private Integer userId;
    private String products;
    private String payMoney;
    private String payChannel;
    private String payTime;
    private Date createTime;
    private Date updateTime;


}
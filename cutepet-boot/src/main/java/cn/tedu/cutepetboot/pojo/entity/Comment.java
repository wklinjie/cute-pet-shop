package cn.tedu.cutepetboot.pojo.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Comment implements Serializable {
    private Integer id;
    private Integer goodsId;
    private Integer repalyId;
    private Integer status;
    private Integer isTop;
    private String nickname;
    private Integer content;
    private String email;
    private String ip;
    private String adress;
    private Integer zan;
}
package cn.tedu.cutepetboot.pojo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class User implements Serializable {
    private Integer id;
    private String username;
    private String password;
    private String nickname;
    private String avatar;
    private Integer age;
    private String sex;
    private String email;
    private Integer phone;
    private String address;
    private Date gmtCreate;
    private Integer loginCount;


}
package cn.tedu.cutepetboot.pojo.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class EvaluationVO implements Serializable {
    private String username;
    private String image;
    private Double qualityScore;
    private String qualityInfo;
    private Integer praise;
}

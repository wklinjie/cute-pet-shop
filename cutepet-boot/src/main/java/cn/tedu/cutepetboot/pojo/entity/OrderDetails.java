package cn.tedu.cutepetboot.pojo.entity;

import lombok.Data;

import javax.sql.rowset.serial.SerialArray;
import java.io.Serializable;
import java.util.Date;
@Data
public class OrderDetails implements Serializable {
    private Integer orderId;
    private Integer userId;
    private Integer goodsId;
    private Integer goodsName;
    private Integer goodsNumber;
    private Integer price;
    private Integer orderMoney;
    private Integer state;
    private String sendAddress;
    private Date createTime;
    private Date updateTime;


}
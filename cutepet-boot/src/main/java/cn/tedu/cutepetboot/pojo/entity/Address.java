package cn.tedu.cutepetboot.pojo.entity;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Address implements Serializable {
    private Integer id;
    private Integer userId;
    private String person;
    private Integer phone;
    private String privince;
    private String city;
    private String country;
    private String address;
    private Integer defaultAddress;
    private Date createTime;
    private Date updateTime;
}
package cn.tedu.cutepetboot.pojo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class PaymentPlatform implements Serializable {
    private Integer id;
    private String name;
    private String info;
    private String state;
    private Date createTime;
    private Date updateTime;


}
package cn.tedu.cutepetboot.pojo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class GoodsCategory implements Serializable {
    private Integer id;
    private String name;
    private Integer parentId;
    private Date createTime;
    private Date updateTime;


}
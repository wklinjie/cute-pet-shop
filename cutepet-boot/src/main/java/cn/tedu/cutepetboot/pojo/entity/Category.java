package cn.tedu.cutepetboot.pojo.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Category implements Serializable {
    private String name;
    private Integer parentId;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}

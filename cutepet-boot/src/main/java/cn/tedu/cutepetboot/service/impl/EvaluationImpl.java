package cn.tedu.cutepetboot.service.impl;

import cn.tedu.cutepetboot.mapper.EvaluationMapper;
import cn.tedu.cutepetboot.pojo.vo.EvaluationVO;
import cn.tedu.cutepetboot.service.IEvaluationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EvaluationImpl implements IEvaluationService {
    @Autowired
    EvaluationMapper mapper;

    @Override
    public List<EvaluationVO> getEvaluationByGoodsId(Integer goodsId) {
        return mapper.getByGoodsId(goodsId);
    }

    @Override
    public Integer getQualityScore(Integer goodsId) {
        return mapper.getQualityScore(goodsId);
    }
}

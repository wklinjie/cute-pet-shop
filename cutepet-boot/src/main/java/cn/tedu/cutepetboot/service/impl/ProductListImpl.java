package cn.tedu.cutepetboot.service.impl;

import cn.tedu.cutepetboot.mapper.ProductListMapper;
import cn.tedu.cutepetboot.pojo.vo.ProductListVO;
import cn.tedu.cutepetboot.service.IProductListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductListImpl implements IProductListService {
    @Autowired
    ProductListMapper mapper;

    @Override
    public List<ProductListVO> getProductByGoodsId(Integer categoryId) {
        return mapper.getProductListByGoodsId(categoryId);
    }
}

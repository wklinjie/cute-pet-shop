package cn.tedu.cutepetboot.service;

import cn.tedu.cutepetboot.pojo.dto.UserRegDTO;
import cn.tedu.cutepetboot.pojo.entity.User;

public interface IUserService {
    // 添加新用户
    void insertUser(UserRegDTO userRegDTO);
}

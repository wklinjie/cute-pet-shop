package cn.tedu.cutepetboot.service;

import cn.tedu.cutepetboot.mapper.CategoryMapper;
import cn.tedu.cutepetboot.pojo.dto.CategoryAddDTO;
import org.springframework.beans.factory.annotation.Autowired;

public interface ICategoryService {
    // 新增商品分类
   void insertCategory(CategoryAddDTO categoryAddDTO);
}

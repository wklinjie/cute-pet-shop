package cn.tedu.cutepetboot.service.impl;

import cn.tedu.cutepetboot.ex.CutepetException;
import cn.tedu.cutepetboot.mapper.CategoryMapper;
import cn.tedu.cutepetboot.pojo.dto.CategoryAddDTO;
import cn.tedu.cutepetboot.pojo.entity.Category;
import cn.tedu.cutepetboot.service.ICategoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
@Service
public class CategoryImpl implements ICategoryService {
    @Autowired
    private CategoryMapper categoryMapper;
    @Override
    public void insertCategory(CategoryAddDTO categoryAddDTO) {
        Category rest = categoryMapper.selectByName(categoryAddDTO.getName());
        if (rest!=null){
            throw new CutepetException("分类已存在");
        }
        Category category = new Category();
        BeanUtils.copyProperties(categoryAddDTO,category);
        LocalDateTime time=LocalDateTime.now();
        category.setCreateTime(time);
        category.setUpdateTime(time);
        categoryMapper.insertCategory(category);
    }
}

package cn.tedu.cutepetboot.service.impl;

import cn.tedu.cutepetboot.mapper.GoodsDetailMapper;
import cn.tedu.cutepetboot.pojo.vo.GoodsDetailVO;
import cn.tedu.cutepetboot.service.IGoodsDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoodsDetailImpl implements IGoodsDetailService {
    @Autowired
    GoodsDetailMapper mapper;
    @Override
    public GoodsDetailVO getGoodsDetailByGoodsId(Integer goodsId) {
        return mapper.getGoodsDetailByGoodsId(goodsId);
    }
}

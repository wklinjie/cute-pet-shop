package cn.tedu.cutepetboot.service;

import cn.tedu.cutepetboot.pojo.vo.GoodsDetailVO;

public interface IGoodsDetailService {
    GoodsDetailVO getGoodsDetailByGoodsId(Integer goodsId);
}

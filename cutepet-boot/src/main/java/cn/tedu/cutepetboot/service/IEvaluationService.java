package cn.tedu.cutepetboot.service;

import cn.tedu.cutepetboot.pojo.vo.EvaluationVO;

import java.util.List;

public interface IEvaluationService {
    List<EvaluationVO> getEvaluationByGoodsId(Integer goodsId);

    Integer getQualityScore(Integer goodsId);
}

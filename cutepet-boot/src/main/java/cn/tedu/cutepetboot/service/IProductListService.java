package cn.tedu.cutepetboot.service;

import cn.tedu.cutepetboot.pojo.vo.ProductListVO;

import java.util.List;

public interface IProductListService {
    List<ProductListVO> getProductByGoodsId(Integer categoryId);
}

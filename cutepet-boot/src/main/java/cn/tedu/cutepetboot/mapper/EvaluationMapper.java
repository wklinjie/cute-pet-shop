package cn.tedu.cutepetboot.mapper;

import cn.tedu.cutepetboot.pojo.vo.EvaluationVO;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface EvaluationMapper {
    List<EvaluationVO> getByGoodsId(Integer goodsId);

    Integer getQualityScore(Integer goodsId);
}

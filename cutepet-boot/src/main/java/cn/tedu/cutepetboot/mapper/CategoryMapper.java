package cn.tedu.cutepetboot.mapper;

import cn.tedu.cutepetboot.pojo.entity.Category;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryMapper {
    // 新增分类
    int insertCategory(Category category);
    // 查询当前所有分类
    Category selectByName(String name);
}

package cn.tedu.cutepetboot.mapper;

import cn.tedu.cutepetboot.pojo.vo.ProductListVO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductListMapper {
    List<ProductListVO> getProductListByGoodsId(Integer categoryId);
}

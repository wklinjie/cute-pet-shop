package cn.tedu.cutepetboot.ex;

public class CutepetException extends RuntimeException{
    public CutepetException() {
    }

    public CutepetException(String message) {
        super(message);
    }

    public CutepetException(String message, Throwable cause) {
        super(message, cause);
    }

    public CutepetException(Throwable cause) {
        super(cause);
    }

    public CutepetException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

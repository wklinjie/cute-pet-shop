package cn.tedu.cutepetboot.controller;

import cn.tedu.cutepetboot.pojo.dto.CategoryAddDTO;
import cn.tedu.cutepetboot.service.ICategoryService;
import cn.tedu.cutepetboot.web.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private ICategoryService categoryService;
    @PostMapping("/add")
    public JsonResult addcategory(@RequestBody CategoryAddDTO categoryAddDTO){
        categoryService.insertCategory(categoryAddDTO);
        return JsonResult.ok("添加成功");
    }
}

package cn.tedu.cutepetboot.controller;

import cn.tedu.cutepetboot.pojo.vo.GoodsDetailVO;
import cn.tedu.cutepetboot.service.IGoodsDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoodsDetailController {
    @Autowired
    IGoodsDetailService service;

    @RequestMapping("/goodsdetail")
    public GoodsDetailVO getGoodsDetailByGoodsId(Integer goodsId){
        return service.getGoodsDetailByGoodsId(goodsId);
    }
}

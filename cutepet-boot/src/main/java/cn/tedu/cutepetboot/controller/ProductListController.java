package cn.tedu.cutepetboot.controller;

import cn.tedu.cutepetboot.pojo.vo.ProductListVO;
import cn.tedu.cutepetboot.service.IProductListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductListController {
    @Autowired
    IProductListService service;

    @RequestMapping("/productlist")
    public List<ProductListVO> getProductListByGoodsId(Integer categoryId){
        return service.getProductByGoodsId(categoryId);
    }
}

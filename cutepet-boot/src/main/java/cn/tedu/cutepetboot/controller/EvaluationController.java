package cn.tedu.cutepetboot.controller;

import cn.tedu.cutepetboot.pojo.vo.EvaluationVO;
import cn.tedu.cutepetboot.service.IEvaluationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EvaluationController {
    @Autowired
    IEvaluationService service;

    @RequestMapping("/evaluation")
    public List<EvaluationVO> getEvaluationByGoodsId(Integer goodsId){
        List<EvaluationVO> evaluationByGoodsId = service.getEvaluationByGoodsId(goodsId);
        return evaluationByGoodsId;
    }

    @RequestMapping("/avgEvaluation")
    public Integer getQualityScore(Integer goodsId){
        return service.getQualityScore(goodsId);
    }
}

package cn.tedu.cutepetboot.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("cn.tedu.cutepetboot.mapper")
public class MybatisConfig {
}

package cn.tedu.cutepetboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CutepetBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(CutepetBootApplication.class, args);
    }

}

import Vue from 'vue'

import VueRouter from 'vue-router'
import Main from '../views/Main.vue'
import layout from '../views/layout/index.vue'
import GoodsManagement from "@/views/layout/goods/GoodsListManagement";
import OrderManagement from "@/views/layout/OrderManagement";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        component: Main
    },
    {
        path: '/login',
        component: () => import('../views/Login.vue')
    },
    {
        path: '/reg',
        component: () => import('../views/RegView.vue')
    },
    {
        path: '/info',
        component: () => import('../views/InfoView.vue')
    },
    {
        path: '/user',
        redirect: '/user/user/info',
        component: () => import('../views/InfoView.vue'),
        children: [
            {
                path: 'user/info',// /user/user/info
                component: () => import('../views/user/UserInfoView.vue'),
                meta: {
                    title: '个人信息中心'
                }
            },
            {
                path: '/user/order/list',// /user/order/list
                component: () => import('../views/user/OrderListView.vue'),
                meta: {
                    title: '订单列表'
                }
            }
        ]
    },
    {
        path: '/goods',
        component: () => import('../views/Goods.vue')
    },
    {
        path: '/doggoods',
        component: () => import('../views/DogGoods.vue')
    }, {
        path: '/catgoods',
        component: () => import('../views/CatGoods.vue')
    }, {
        path: '/suigoods',
        component: () => import('../views/SuiGoods.vue')
    },
    {
        path: '/about',
        component: () => import('../views/AboutView.vue')
    },
    {
        path: '/admin',
        component: layout,
        children: [
            {
                path: '/adminHome',
                name: 'AdminHome',
                component: () => import('../views/layout/AdminHome.vue'),
            },
            {
                path: '/admin/goodsManagement',
                redirect: '/goodsManagement/goods-list',
                component: {
                    render(c) {
                        return c('router-view')
                    }
                },
                children: [
                    {
                        path: '/goodsManagement/goods-list',
                        component: () => import('../views/layout/goods/GoodsListManagement.vue')
                    },

                    {
                        path: '/goodsManagement/goods-category',
                        component: () => import('../views/layout/goods/GoodsCategory.vue')
                    }
                ]
            },
        ]
    },
    {
        path: '/add-goods',
        name: 'AddGoods',
        component: () => import('../views/layout/goods/AddGoods.vue')
    },
    {
        path: '/orders',
        name: 'OrderManagement',
        component: () => import('../views/layout/OrderManagement.vue'),
    }, {
        path: '/adminInfo',
        name: 'AdminInfo',
        component: () => import('../views/layout/AdminInfo.vue'),
    },
    {
        path: '/productdetailinfo',
        component: () => import('../views/ProductDetailInfoView.vue')
    },
]


const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
